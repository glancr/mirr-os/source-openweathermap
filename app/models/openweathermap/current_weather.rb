# frozen_string_literal: true

module Openweathermap
  class CurrentWeather < GroupSchemas::CurrentWeather
  end
end
