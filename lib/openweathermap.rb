# frozen_string_literal: true

require 'openweathermap/engine'
require 'httparty'

module Openweathermap
  class Hooks
    REFRESH_INTERVAL = '30m'
    OWM_API = 'https://api.openweathermap.org/data/2.5'

    # @return [String]
    def self.refresh_interval
      REFRESH_INTERVAL
    end

    # @param [Hash] configuration
    def initialize(instance_id, configuration)
      @instance_id = instance_id
      @temperature_unit = configuration['temperatureUnit']
      @api_key = configuration['apiKey']
      @city_id = configuration['cityId']
      @city_name = configuration['cityName']
    end

    def default_title
      @city_name
    end

    def validate_configuration
      raise ArgumentError, 'invalid city ID' if @city_id.blank?

      res = HTTParty.head("#{OWM_API}/weather?id=#{@city_id}&units=metric&lang=EN&APPID=#{
        @api_key}")

      if res.code >= 400
        field, message = case res.code
                         when 401
                           ['apiKey', 'invalid API key']
                         when 400, 404
                           ['cityId', 'city ID does not exist in Openweathermap database.']
                         else
                           ['', 'unknown error']
                         end
        raise ArgumentError, "#{field} – #{message}"
        #raise ExtensionConfigValidation::SourceInstanceValidationError.new(field), message
      end
    end

    def configuration_valid?
      res = HTTParty.head("#{OWM_API}/weather?id=#{@city_id}&units=metric&lang=EN&APPID=#{@api_key}")
      res.success?
    rescue StandardError => e
      Rails.logger.error "Error fetching Openweathermap data for validation: #{e.message}"
      false
    end

    # @return [Array<Array<Integer, String>>] A list of [city_id, city_name]
    def list_sub_resources
      [[@city_id, @city_name]]
    end

    def fetch_data(group, _sub_resources)
      # https://samples.openweathermap.org/data/2.5/forecast?id=524901&appid=b6907d289e10d714a6e88b30761fae22
      res = HTTParty.get("#{OWM_API}/forecast?id=#{@city_id}&units=#{@temperature_unit}&APPID=#{@api_key}")
      raise HTTParty::ResponseError, "openweathermap API returned #{res.code}: #{res.body}" unless res.success?

      body = res.parsed_response
      raise StandardError, "API didn't return any forecasts" if body['list'].blank?

      case group
      when 'weather_owm'
        weather_owm = ::Openweathermap::WeatherOwm.find_or_initialize_by(id: @city_id)

        weather_owm.update(
          location_name: @city_name,
          sunrise: DateTime.strptime(body['city']['sunrise'].to_s, '%s'),
          sunset: DateTime.strptime(body['city']['sunset'].to_s, '%s')
        )

        body['list'].each do |forecast|
          utc_time = Time.find_zone('UTC').parse(forecast['dt_txt'])
          weather_owm.entries.find_or_initialize_by(dt_txt: utc_time).update(
            unit: @temperature_unit,
            forecast: forecast
          )
        end

        # OWM returns timestamps in UTC time
        weather_owm.entries.each { |e| e.mark_for_destruction if e.dt_txt < Time.current.utc }

        [weather_owm]

      when 'current_weather'
        current_weather = ::Openweathermap::CurrentWeather.find_or_initialize_by(id: @city_id)

        current_weather.update(
          station_name: @city_name,
          location: @city_name
        )

        current_conditions = body['list'].first
        current_weather.entries.find_or_initialize_by(uid: current_conditions['dt_txt']).update(
          temperature: current_conditions['main']['temp'],
          humidity: current_conditions['main']['humidity'],
          air_pressure: current_conditions['main']['pressure'],
          wind_speed: current_conditions['wind']['speed'],
          wind_angle: current_conditions['wind']['deg'],
          rain_last_hour: nil,
          condition_code: current_conditions['weather'].first.try(:[], 'icon'),
          unit: @temperature_unit,
          pop: current_conditions['pop']&.to_d
        )

        # OWM returns timestamps in UTC time
        current_weather.entries.each { |e| e.mark_for_destruction unless e.uid.eql?(current_conditions['dt_txt']) }

        [current_weather]
      else
        Rails.logger.error "#{__method__}: received unknown group #{group}"
      end
    end

  end
end
